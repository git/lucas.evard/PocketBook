# PocketBook - Projet MVVM

## Organisation

- Les pages sont disponibles dans le dossier [_**Pages**_]("https://codefirst.iut.uca.fr/git/lucas.evard/PocketBook/src/branch/master/sources/PocketBook/Pages"). 
- Les ContentView réutilisé dans les pages se trouvent dans le dossier [_**Composants/ContentView**_]("https://codefirst.iut.uca.fr/git/lucas.evard/PocketBook/src/branch/master/sources/PocketBook/Pages/Composants/ContentView").
- Les VM applicatives sont à l'intérieur du projet MAUI, dans le dossier [_**Applicative_VM**_]("https://codefirst.iut.uca.fr/git/lucas.evard/PocketBook/src/branch/master/sources/PocketBook/Applicative_VM").
- Les VM wrappant les classes du modèle sont présent dans une bibliothèque de classes externe au projet MAUI, dans le dossier [_**ViewModel**_]("https://codefirst.iut.uca.fr/git/lucas.evard/PocketBook/src/branch/master/sources/ViewModel").

Il n'y a aucune dépendance entre le modèle et la vue.

## Mon toolkit MVVM

Un toolkit MVVM a été mis en place afin de soulager les écritures redondantes pour wrapper le modèle et pour la gestion des modification de propriétées.

Intégration du toolkit au projet => Package MyToolkitMVVM :

```mermaid
classDiagram

class BaseViewModel~T~{
    +TModel Model
    -TModel model
    +BaseViewModel(model: TModel)
    +BaseViewModel():this(default(TModel))
}

class BaseViewModel{
    + BaseViewModel()
}

class ObservableObject{
    +PropertyChangedEventHandler? PropertyChanged;
    #OnPropertyChanged(propertyName: string)
    #SetProperty~T~(member: T ref, value: T, propertyName: string)
    #SetProperty~T~(member: T, value: T,action Action~T~, propertyName: string)
}

class RelayCommand~T~ {
+CanExecuteChanged: EventHandler
-execute: Action<T>
-canExecute: Func<T, bool>
+RelayCommand(Action<T> execute, Func<T, bool> canExecute)
+CanExecute(object? parameter): bool
+Execute(object? parameter): void
}

class RelayCommand {
-execute: Action<object>
-canExecute: Func<object, bool>
+RelayCommand(Action<object> execute, Func<object, bool> canExecute)
+Execute(): Task
}


class INotifyPropertyChanged{
    <<interface>>
}

class ICommand{
    <<interface>>
}


RelayCommand --|> ICommand
ObservableObject --|> INotifyPropertyChanged
BaseViewModel --|> ObservableObject

```

Ensuite, il suffit de choisir s'il faut hériter de BaseViewModel<T> qui est générique, ou BaseViewModel. 

Pour ma part, mes VM wrapper héritent automatiquement de BaseViewModel<T> avec T désignant la classe du modèle à wrapper. 

Cependant, mes VM applicatives n'héritent pas forcément de BaseViewModel, je le fais hériter seulement lorsqu'une propriétée doit être bindée dans la vue, comme pour ButtonsVM.

## Vue

Concernant la vue, j'ai repris certains de vos composants: 
- Prêt/Emprunts => le bouton en forme de switch
- Le bouton pour scanner un ISBN ou le saisir

J'ai repris certaines de mes vues afin de mettre en place la ToolbarItem.

La note des livres sont ramenée en nombre entier le plus proche. Ce qui implique que les étoiles sont affichées seulement pleines ou vides.

## Bugs

En jouant avec la liste de livres, et en naviguant sur d'autres pages (tous,favoris,filtres et à lire plus tard), il est possible que le rendu des étoiles dans le détail des livres soit faux.

Si on ne commence pas l'affichage de la liste de livres par la page **tous**, il est possible que la liste s'affiche avec une hauteur très réduite. 

## Ce qui est implémenté

En prenant compte votre Readme, il y a :
- La navigation en utilisant des **Commands**
- Mise en place de **ContentView **
- Affichage de la **listes de livres** et **sélection d'un livre**
- Affichage entièrement bindé des **détails d'un livre**
- **Filtrage** par Auteur,Note et Date de publication
- **Changement de statut** d'un livre
- **Ajout d'un livre aux favoris**, il suffit d'aller dans l'onglet déplacer un livre et appuyer sur '_Déplacer en favoris_'
- Ajout d'un livre par ISBN
- **Supprimer un livre**, il suffit d'aller dans l'onglet déplacer un livre et appuyer sur '_Mettre à la poubelle_'
- Gestion de la **pagination**

## Ce qui n'as pas été implémenté

En prenant compte votre Readme, il y a :
- **Prêt** d'un livre
- Utilisation du **MVVM Community Toolkit**
- **Scanner ISBN**
