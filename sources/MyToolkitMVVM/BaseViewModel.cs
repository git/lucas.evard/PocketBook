﻿ using System;
namespace MyToolkitMVVM
{

    public class BaseViewModel<TModel> : ObservableObject
    {
        public TModel Model {
            get => model;
            set => SetProperty(ref model,value);
        }
        private TModel model;

        public BaseViewModel(TModel model)
        {
            Model = model;
        }
        public BaseViewModel()
            : this(default(TModel))
        {}
    }


    public class BaseViewModel : ObservableObject
    {
		public BaseViewModel()
		{
		}
	}

}

