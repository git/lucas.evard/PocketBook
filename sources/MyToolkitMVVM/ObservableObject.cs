﻿using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace MyToolkitMVVM;

public class ObservableObject : INotifyPropertyChanged
{
    public event PropertyChangedEventHandler? PropertyChanged;

    protected void OnPropertyChanged(string propertyName)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    protected void SetProperty<T>(ref T member, T value, [CallerMemberName] string propertyName = null)
    {
        if (EqualityComparer<T>.Default.Equals(member, value)) return;
        member = value;
        OnPropertyChanged(propertyName);
        Debug.WriteLine("PropertyChanged "+propertyName);
    }
    protected void SetProperty<T>(T member, T value,Action<T> action, [CallerMemberName] string propertyName = null)
    {
        if (EqualityComparer<T>.Default.Equals(member, value)) return;
        action(value);
        OnPropertyChanged(propertyName);
        Debug.WriteLine("PropertyChanged " + propertyName);
    }
}
