﻿using System;
using System.Windows.Input;

namespace MyToolkitMVVM
{
	public class RelayCommand<T> : ICommand
	{
        public event EventHandler? CanExecuteChanged;

        private readonly Action<T> execute;
        private readonly Func<T, bool> canExecute;

        public RelayCommand(Action<T> execute, Func<T, bool> canExecute = null)
        {
            this.execute = execute;
            this.canExecute = canExecute;
        }

        public bool CanExecute(object? parameter)
        {
            if (parameter == null)
                return true;
            return canExecute((T)parameter);
        }

        public void Execute(object? parameter = null)
        {
            execute((T)parameter);
        }
    }
    public class RelayCommand : RelayCommand<object>
    {
        public RelayCommand(Action<object> execute, Func<object, bool> canExecute = null)
            : base(execute, canExecute)
        {
        }

       /* public async Task<bool> CanExecuteAsync(object? parameter)
        {
            if (parameter == null)
                return true;
            return await CanExecuteInternalAsync((T)parameter);
        }*/

        public async Task Execute()
        {
            await Execute();
        }
    }
}

