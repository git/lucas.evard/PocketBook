﻿namespace PocketBook;

public partial class AppShell : Shell
{
	public AppShell()
	{
		Routing.RegisterRoute("TousPage",typeof(TousPage));
        Routing.RegisterRoute("StatutPage", typeof(StatutPage));
        Routing.RegisterRoute("SharePage", typeof(SharePage));
        Routing.RegisterRoute("LaterPage", typeof(LaterPage));
        Routing.RegisterRoute("Filter", typeof(Filter));
        Routing.RegisterRoute("BookDetail", typeof(BookDetail));
        InitializeComponent();
	}
}
