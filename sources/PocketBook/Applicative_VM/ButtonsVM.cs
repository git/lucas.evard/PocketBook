﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using MyToolkitMVVM;
using ViewModel;

namespace PocketBook.Applicative_VM
{
    public class ButtonsVM : BaseViewModel
    {
        public ICommand ShowScanMenu { get; private set; }
        public ICommand ModalISBN { get; private set; }
        public ManagerVM Manager { get; private set; }
        public PaginationVM PaginationVM { get; private set; }

        private bool scanMenuIsVisible;

        public bool ScanMenuIsVisible
        {
            get => scanMenuIsVisible;
            set => SetProperty(ref scanMenuIsVisible,value);   
        }

        public ButtonsVM(ManagerVM manager, PaginationVM paginationVM)
        {
            Manager = manager;
            PaginationVM = paginationVM;
            ShowScanMenu = new Command(() =>
            {
                ScanMenuIsVisible = !ScanMenuIsVisible;
            });
            ModalISBN = new Command(
                async () => {
                    string isbn = await App.Current.MainPage.DisplayPromptAsync("Ajout d'un livre par ISBN", "Entrez l'ISBN présent sur votre livre pour l'ajouter à votre librarie");
                    if (isbn != null) {
                        Manager.AddBookByISBN(isbn);
                        PaginationVM.Refresh();
                    }
                }
            ); ;
        }
    }
}
