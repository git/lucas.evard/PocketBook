﻿using CommunityToolkit.Maui.Alerts;
using System.Windows.Input;
using MyToolkitMVVM;
using ViewModel;
using CommunityToolkit.Maui.Core;

namespace PocketBook.Applicative_VM
{
	public class DetailPageVM : BaseViewModel
	{
        public ManagerVM Manager { get; private set; }
        public PaginationVM PaginationVM { get; private set; }

        public ICommand ChangeStatus { get; private set; }
        public ICommand AddReadLaterList { get; private set; }
        public ICommand MoveBook { get; private set; }

        public DetailPageVM(ManagerVM manager, PaginationVM paginationVM)
		{
			Manager = manager;
            PaginationVM = paginationVM;

            ChangeStatus = new Command(async o =>
			{
                string action = await App.Current.MainPage.DisplayActionSheet("Selectionne un statut de lecture", "Retour", null ,"À lire", "En lecture", "Non lu","Terminé");
                Manager.ChangeStatut(action);
                _ = Toast.Make("Changement du status: " + Manager.SelectedBook.BookStatus, ToastDuration.Short, 14).Show(new CancellationTokenSource().Token);
            });

            AddReadLaterList = new Command( o =>
            {
                Manager.ChangeStatut("À lire");
                _ = Toast.Make("Ajout dans la liste de livre à lire plus tard", ToastDuration.Short, 14).Show(new CancellationTokenSource().Token);
            });
            MoveBook = new Command(async o =>
            {
                string action;
                if (PaginationVM.Page == "Like")
                {
                    action = await App.Current.MainPage.DisplayActionSheet("Selectionne où tu veux déplacer ce livre", "Retour", "Mettre à la poubelle", "Supprimer des favoris");
                }
                else action = await App.Current.MainPage.DisplayActionSheet("Selectionne où tu veux déplacer ce livre", "Retour", "Mettre à la poubelle", "Déplacer en favoris");

                Manager.MoveBook(action);
                _ = Toast.Make("Confirmation pour l'action:  " + action, ToastDuration.Short, 14).Show(new CancellationTokenSource().Token);
                if (action == "Mettre à la poubelle" || action== "Supprimer des favoris") 
                {
                    PaginationVM.Refresh();
                    await App.Current.MainPage.Navigation.PopAsync();
                }
            });
        }
	}
}

