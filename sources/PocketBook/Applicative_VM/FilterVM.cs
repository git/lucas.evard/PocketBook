﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ViewModel;

namespace PocketBook.Applicative_VM
{
    public class FilterVM
    {
        public ICommand AuthorCommand { get; private set; }
        public ICommand MarkCommand { get; private set; }
        public ICommand YearCommand { get; private set; }
        public ICommand FilterCommand { get; private set; }
        public ICommand FilterLoadDataAuthor { get; private set; }
        public ICommand FilterLoadDataYear { get; private set; }
        public ICommand FilterLoadDataMark { get; private set; }

        public ManagerVM Manager { get; private set; }
        public NavigatorVM Navigator { get; private set; }

        public FilterVM(ManagerVM manager,NavigatorVM navigator) 
        { 
            Manager = manager;
            Navigator = navigator;
            string currentRoute = (Application.Current.MainPage as NavigationPage)?.CurrentPage?.ToString();
            Debug.WriteLine(currentRoute);
            FilterCommand = new Command<string>(option =>
            {
                switch (option)
                {
                    case "Author":
                        AuthorCommand.Execute(null);
                        break;
                    case "Mark":
                        MarkCommand.Execute(null);
                        break;
                    case "Year":
                        YearCommand.Execute(null);
                        break;
                }
            });
            AuthorCommand = new Command(async () =>
            {
                await Manager.GetAuthorFromCollection();
            });
            YearCommand = new Command( async () =>
            {
                await Manager.GetDatesFromCollection();
            });
            MarkCommand = new Command(async () =>
            {
                await Manager.GetMarkFromCollection();
            });
            FilterLoadDataAuthor = new Command<string>(async type =>
            {
                Manager.Index = 0;
                await Manager.GetBooksFromAuthor(type);
            });
            FilterLoadDataMark = new Command<string>(async type =>
            {
                Manager.Index = 0;
                await Manager.GetBooksFromMark(type);
            });
            FilterLoadDataYear = new Command<string>(async type =>
            {
                Manager.Index = 0;
                await Manager.GetBooksFromYears(type);
            });
        }


    }
}
