﻿using System;
using System.Windows.Input;
using ViewModel;

namespace PocketBook.Applicative_VM
{
	public class LikeVM
	{
        public ManagerVM Manager { get; private set; }
		public NavigatorVM NavigatorVM { get; private set; }
        public PaginationVM PaginationVM { get; private set; }

        public ICommand NavigateAndLoad { get; private set; }

        public LikeVM(ManagerVM manager, NavigatorVM navigatorVM,PaginationVM paginationVM)
		{
			PaginationVM = paginationVM;
			Manager = manager;
			NavigatorVM = navigatorVM;

			NavigateAndLoad = new Command( async o =>
			{
				Manager.Index = 0;
                PaginationVM.Page = "Like";
                await Manager.GetFavoriteBooks();
				PaginationVM.Refresh();
                NavigatorVM.Navigateto.Execute("TousPage");
            });
		}
	}
}

