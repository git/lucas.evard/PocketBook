﻿using System;
using System.Windows.Input;

namespace PocketBook.Applicative_VM
{
	public class NavigatorAndFilterVM
	{
        public NavigatorVM NavigateCommandBooks { get; private set; }
        public FilterVM FilterVM { get; private set; }
        public ICommand NavigateAndLoad { get; private set; }
        public ICommand NavigateAndLoadData { get; private set; }

        public string ActualTypeFilter { get; private set; }

        public NavigatorAndFilterVM(FilterVM filterVM, NavigatorVM navigator)
		{
            NavigateCommandBooks = navigator;
            FilterVM = filterVM;
            NavigateAndLoad = new Command<string>(o =>
            {
                ActualTypeFilter = o;
                FilterVM.FilterCommand.Execute(o);
                NavigateCommandBooks.Navigateto.Execute("Filter");
            });
            NavigateAndLoadData = new Command<string>(o =>
            {
                switch (ActualTypeFilter)
                {
                    case "Author":
                        FilterVM.FilterLoadDataAuthor.Execute(o);;
                        break;
                    case "Mark":
                        FilterVM.FilterLoadDataMark.Execute(o); ;
                        break;
                    case "Year":
                        FilterVM.FilterLoadDataYear.Execute(o); ;
                        break;
                }
                NavigateCommandBooks.Navigateto.Execute("TousPage");
            });
        }
	}
}

