﻿using System;
using System.Windows.Input;
using System.Threading.Tasks;
using System.Reflection;
using ViewModel;

namespace PocketBook.Applicative_VM
{
    public class NavigatorVM
    {
        public ICommand Navigateto { get; private set; }
        public ICommand NavigatetoFilter { get; private set; }

        public NavigatorVM()
        {
            Navigateto = new Command<string>(execute:async page => {
                await Shell.Current.GoToAsync(page);
            });

        }
    }
}
