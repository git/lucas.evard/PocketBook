﻿using System;
using System.Diagnostics;
using System.Windows.Input;
using ViewModel;

namespace PocketBook.Applicative_VM
{
	public class PaginationVM
	{
        public ManagerVM Manager { get; private set; }
        public ICommand NormalIncreasePage { get; private set; }
        public ICommand NormalDecreasePage { get; private set; }
        public string Page;


        public void Refresh()
        {
            ((Command)NormalIncreasePage).ChangeCanExecute();
            ((Command)NormalDecreasePage).ChangeCanExecute();
        }

        public PaginationVM(ManagerVM managerVM)
		{
			Manager = managerVM;
            NormalIncreasePage = new Command(execute:async () =>
            {
                Manager.Index++;
                switch (Page)
                {
                    case "TousPage":
                        await Manager.LoadBooksFromManager();
                        break;
                    case "Later":
                        await Manager.LoadBooksReadLaterFromManager();
                        break;
                    case "Like":
                        await Manager.GetFavoriteBooks();
                        break;
                }
                Refresh();
            },
            canExecute: () => {
                return Manager.NbBook / Manager.NBLIVREPARPAGE > Manager.Index;
            });

            NormalDecreasePage = new Command(execute:async() =>
            {
                if (Manager.Index > 0)
                {
                    Manager.Index--;
                    switch (Page)
                    {
                        case "TousPage":
                            await Manager.LoadBooksFromManager();
                            break;
                        case "Later":
                            await Manager.LoadBooksReadLaterFromManager();
                            break;
                        case "Like":
                            await Manager.GetFavoriteBooks();
                            break;
                    }
                    Refresh();
                }
            },
            canExecute: () => {
                return Manager.Index > 0;
            });
        }
	}
}

