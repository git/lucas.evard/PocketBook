﻿using System;
using System.Diagnostics;
using System.Windows.Input;
using MyToolkitMVVM;
using ViewModel;

namespace PocketBook.Applicative_VM
{
	public class TousPageVM : BaseViewModel
    {
        public ICommand NavigateAndLoadData { get; private set; }
        public ICommand NavigateDetailPage { get; private set; }

        public NavigatorVM NavigateCommandBooks { get; private set; }
        public ButtonsVM ButtonsVM { get; private set; }
        public ManagerVM Manager { get; private set; }
        public PaginationVM PaginationVM { get; private set; }


        public TousPageVM(NavigatorVM navigation,ManagerVM manager, ButtonsVM buttonsVM, PaginationVM paginationVM)
        {
            ButtonsVM = buttonsVM;
            Manager = manager;
            PaginationVM = paginationVM;
            NavigateCommandBooks = navigation;
            NavigateDetailPage = new Command<BookVM>(book =>
            {
                Manager.SelectedBook = book;
                NavigateCommandBooks.Navigateto.Execute("BookDetail");
            });
            NavigateAndLoadData = new Command(async o =>
            {
                Manager.Index = 0;
                PaginationVM.Page = "TousPage";
                await Manager.LoadBooksFromManager();
                PaginationVM.Refresh();
                NavigateCommandBooks.Navigateto.Execute("TousPage");
            });
		}
	}
}

