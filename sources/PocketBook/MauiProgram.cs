﻿using Microsoft.Extensions.Logging;
using CommunityToolkit.Maui;
using PocketBook.Applicative_VM;
using ViewModel;
using StubLib;
using Model;

namespace PocketBook;

public static class MauiProgram
{
	public static MauiApp CreateMauiApp()
	{
        var builder = MauiApp.CreateBuilder();
        builder
            .UseMauiApp<App>()
            .UseMauiCommunityToolkit()
            .ConfigureFonts(fonts =>
            {
                fonts.AddFont("SF-Compact-Display-Black.otf", "Compact-Display-Black");
                fonts.AddFont("SF-Compact-Display-Bold.otf", "Compact-Display-Bold");
                fonts.AddFont("SF-Compact-Display-Heavy.otf", "Compact-Display-Heavy");
                fonts.AddFont("SF-Compact-Display-Semibold.otf", "Compact-Display-Semibold");

                fonts.AddFont("SF-Pro-Display-Black.otf", "Pro-Display-Black");
                fonts.AddFont("SF-Pro-Display-Bold.otf", "Pro-Display-Bold");
                fonts.AddFont("SF-Pro-Display-Heavy.otf", "Pro-Display-Heavy");
                fonts.AddFont("SF-Pro-Display-Semibold.otf", "Pro-Display-Semibold");

                fonts.AddFont("SF-Compact-Text-Black.otf", "Compact-Text-Black");
                fonts.AddFont("SF-Compact-Text-Bold.otf", "Compact-Text-Bold");
                fonts.AddFont("SF-Compact-Text-Heavy.otf", "Compact-Text-Heavy");
                fonts.AddFont("SF-Compact-Text-Semibold.otf", "Compact-Text-Semibold");

                fonts.AddFont("SF-Pro-Text-Black.otf", "Pro-Text-Black");
                fonts.AddFont("SF-Pro-Text-Bold.otf", "Pro-Text-Bold");
                fonts.AddFont("SF-Pro-Text-Heavy.otf", "Pro-Text-Heavy");
                fonts.AddFont("SF-Pro-Text-Semibold.otf", "Pro-Text-Semibold");

                fonts.AddFont("SFUIDisplay-Light.ttf", "SF-Light");
                fonts.AddFont("SFUIDisplay-Bold.ttf", "SF-Bold");

            });
        builder.Services
            .AddSingleton<ILibraryManager, LibraryStub>()
            .AddSingleton<IUserLibraryManager, UserLibraryStub>()
            .AddSingleton<NavigatorVM>()
            .AddSingleton<ButtonsVM>()
            .AddSingleton<TousPageVM>()
            .AddSingleton<MainPage>()
            .AddSingleton<NavigatorAndFilterVM>()
            .AddSingleton<TousPage>()
            .AddSingleton<BookDetail>()
            .AddSingleton<DetailPageVM>()
            .AddSingleton<PaginationVM>()
            .AddSingleton<LaterPageVM>()
            .AddSingleton<LaterPage>()
            .AddSingleton<FilterVM>()
            .AddSingleton<SharePage>()
            .AddSingleton<ManagerVM>()
            .AddSingleton<StatutPage>()
            .AddSingleton<Filter>()
            .AddSingleton<LikeVM>()

        ;
#if DEBUG
        builder.Logging.AddDebug();
#endif

		return builder.Build();
	}
}
