using PocketBook.Applicative_VM;

namespace PocketBook;

public partial class BookDetail : ContentPage
{

    public DetailPageVM DetailPageVM { get; private set; }

    public BookDetail(DetailPageVM detailPageVM)
    {
        InitializeComponent();
        DetailPageVM = detailPageVM;
        BindingContext = DetailPageVM;
    }
}