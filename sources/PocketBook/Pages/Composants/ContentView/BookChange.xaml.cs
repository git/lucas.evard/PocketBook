using System.Windows.Input;

namespace PocketBook;

public partial class BookChange : ContentView
{

    public static readonly BindableProperty ImageNameProperty=
    BindableProperty.Create(nameof(ImageName), typeof(string), typeof(BookChange), string.Empty);

    public static readonly BindableProperty PropertyNameProperty =
    BindableProperty.Create(nameof(PropertyName), typeof(string), typeof(BookChange), string.Empty);


    public static readonly BindableProperty CommandProperty =
        BindableProperty.Create(nameof(Command), typeof(ICommand), typeof(BookChange));

    public ICommand Command
    {
        get { return (ICommand)GetValue(CommandProperty); }
        set { SetValue(CommandProperty, value); }
    }

    public string ImageName
    {
        get { return (string)GetValue(ImageNameProperty); }
        set { SetValue(ImageNameProperty, value); }
    }

    public string PropertyName
    {
        get { return (string)GetValue(PropertyNameProperty); }
        set { SetValue(PropertyNameProperty, value); }
    }

    public BookChange()
	{
		InitializeComponent();
        imageName.SetBinding(Image.SourceProperty, new Binding(nameof(ImageName), source: this));
        propertyName.SetBinding(Label.TextProperty, new Binding(nameof(PropertyName), source: this));
    }
}