using System.Windows.Input;

namespace PocketBook;
public partial class ContentViewBook : ContentView
{
    
    public static readonly BindableProperty BookNameProperty =
        BindableProperty.Create(nameof(BookName), typeof(string), typeof(ContentViewBook), string.Empty);

    public static readonly BindableProperty AutorNameProperty =
        BindableProperty.Create(nameof(AutorName), typeof(string), typeof(ContentViewBook), string.Empty);

    public static readonly BindableProperty BookStatusProperty =
        BindableProperty.Create(nameof(BookStatus), typeof(string), typeof(ContentViewBook), string.Empty);

    public static readonly BindableProperty BookImageProperty =
        BindableProperty.Create(nameof(BookImage), typeof(string), typeof(ContentViewBook), string.Empty);

    public static readonly BindableProperty CommandProperty =
    BindableProperty.Create(nameof(Command), typeof(ICommand), typeof(ContentViewBook));

    public static readonly BindableProperty CommandParameterProperty =
        BindableProperty.Create(nameof(CommandParameter), typeof(string), typeof(ContentViewBook));

    public static readonly BindableProperty NombreEtoileProperty =
BindableProperty.Create(nameof(NombreEtoile), typeof(int), typeof(ContentViewBook), defaultValue: 0);

    public int NombreEtoile
    {
        get { return (int)GetValue(NombreEtoileProperty); }
        set { SetValue(NombreEtoileProperty, value); }
    }

    public ICommand Command
    {
        get { return (ICommand)GetValue(CommandProperty); }
        set { SetValue(CommandProperty, value); }
    }

    public string CommandParameter
    {
        get { return (string)GetValue(CommandParameterProperty); }
        set { SetValue(CommandParameterProperty, value); }
    }

    public string BookName
    {
        get { return (string)GetValue(BookNameProperty); }
        set { SetValue(BookNameProperty, value); }
    }
    public string BookImage
    {
        get { return (string)GetValue(BookImageProperty); }
        set { SetValue(BookImageProperty, value); }
    }

    public string AutorName
    {
        get { return (string)GetValue(AutorNameProperty); }
        set { SetValue(AutorNameProperty, value); }
    }
    public string BookStatus
    {
        get { return (string)GetValue(BookStatusProperty); }
        set { SetValue(BookStatusProperty, value); }
    }
    public ContentViewBook()
	{
		InitializeComponent();
    }
}