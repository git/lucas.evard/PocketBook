using System.Windows.Input;
using CommunityToolkit.Maui.Behaviors;

namespace PocketBook
{
    public partial class ContentViewFilter : ContentView
    {
        public static readonly BindableProperty FilterNumProperty =
            BindableProperty.Create(nameof(FilterNum), typeof(string), typeof(ContentViewFilter), string.Empty);

        public static readonly BindableProperty FilterImageNameProperty =
            BindableProperty.Create(nameof(FilterImageName), typeof(string), typeof(ContentViewFilter), string.Empty);

        public static readonly BindableProperty FilterNameProperty =
            BindableProperty.Create(nameof(FilterName), typeof(string), typeof(ContentViewFilter), string.Empty);

        public static readonly BindableProperty CommandProperty =
            BindableProperty.Create(nameof(Command), typeof(ICommand), typeof(ContentViewFilter));

        public static readonly BindableProperty CommandParameterProperty =
            BindableProperty.Create(nameof(CommandParameter), typeof(string), typeof(ContentViewFilter));

        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        public string CommandParameter
        {
            get { return (string)GetValue(CommandParameterProperty); }
            set { SetValue(CommandParameterProperty, value); }
        }

        public string FilterImageName
        {
            get { return (string)GetValue(FilterImageNameProperty); }
            set { SetValue(FilterImageNameProperty, value); }
        }

        public string FilterName
        {
            get { return (string)GetValue(FilterNameProperty); }
            set { SetValue(FilterNameProperty, value); }
        }

        public string FilterNum
        {
            get { return (string)GetValue(FilterNumProperty); }
            set { SetValue(FilterNumProperty, value); }
        }

        public ContentViewFilter()
        {
            InitializeComponent();
        }
    }
}
