﻿using System.Windows.Input;

namespace PocketBook;

public partial class Filtrage : ContentView
{

    public static readonly BindableProperty DatanameProperty =
    BindableProperty.Create(nameof(Dataname), typeof(string), typeof(Filtrage), string.Empty);

    public static readonly BindableProperty NumberDataProperty =
        BindableProperty.Create(nameof(NumberData), typeof(string), typeof(Filtrage), string.Empty);

    public static readonly BindableProperty CommandProperty =
        BindableProperty.Create(nameof(Command), typeof(ICommand), typeof(Filtrage));

    public static readonly BindableProperty CommandParameterProperty =
            BindableProperty.Create(nameof(CommandParameter), typeof(string), typeof(Filtrage));

    public string CommandParameter
    {
        get { return (string)GetValue(CommandParameterProperty); }
        set { SetValue(CommandParameterProperty, value); }
    }

    public ICommand Command
    {
        get { return (ICommand)GetValue(CommandProperty); }
        set { SetValue(CommandProperty, value); }
    }

    public string Dataname
    {
        get { return (string)GetValue(DatanameProperty); }
        set { SetValue(DatanameProperty, value); }
    }
    public string NumberData
    {
        get { return (string)GetValue(NumberDataProperty); }
        set { SetValue(NumberDataProperty, value); }
    }



    public Filtrage()
	{
		InitializeComponent();
    }
}
