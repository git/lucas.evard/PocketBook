﻿using System.Windows.Input;

namespace PocketBook;

public partial class ScanMenuView : ContentView
{
    public static readonly BindableProperty CommandProperty =
        BindableProperty.Create(nameof(Command), typeof(ICommand), typeof(ScanMenuView));

    public ICommand Command
    {
        get { return (ICommand)GetValue(CommandProperty); }
        set { SetValue(CommandProperty, value); }
    }

    public ScanMenuView()
	{
		InitializeComponent();
    }

}
