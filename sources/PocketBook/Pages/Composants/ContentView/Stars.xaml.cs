using CommunityToolkit.Maui.Behaviors;
using Microsoft.Maui.Controls;
using System.Linq;

namespace PocketBook
{
    public partial class Stars : ContentView
    {
        public static readonly BindableProperty NombreEtoileProperty =
            BindableProperty.Create(nameof(NombreEtoile), typeof(int), typeof(Stars), defaultValue: 0, propertyChanged: OnNbStarsChanged);

        public int NombreEtoile
        {
            get => (int)GetValue(NombreEtoileProperty);
            set => SetValue(NombreEtoileProperty, value);
        }

        public Stars()
        {
            InitializeComponent();
            InitializeStars();
        }

        private void InitializeStars()
        {
            var behaviorGrey = new IconTintColorBehavior
            {
                TintColor = Color.FromArgb("#E6E6E6"),
            };

            foreach (Image i in StarsList.Children.OfType<Image>())
            {
                i.Behaviors.Add(behaviorGrey);
            }
        }

        private static void OnNbStarsChanged(BindableObject bindable, object oldValue, object newValue)
        {
            if (bindable is Stars stars)
            {
                stars.UpdateStars();
            }
        }

        private void UpdateStars()
        {
            int z = 0;

            var behaviorGrey = new IconTintColorBehavior
            {
                TintColor = Color.FromArgb("#E6E6E6"),
            };

            var behaviorYellow = new IconTintColorBehavior
            {
                TintColor = Color.FromArgb("#FFFF00"),
            };

            foreach (Image i in StarsList.Children.OfType<Image>())
            {
                if (z < NombreEtoile)
                {
                    i.Behaviors.Add(behaviorYellow);
                }
                else
                {
                    i.Behaviors.Add(behaviorGrey);
                }
                z++;
            }
        }
    }
}
