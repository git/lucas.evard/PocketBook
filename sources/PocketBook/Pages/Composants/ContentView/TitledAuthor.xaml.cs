namespace PocketBook;

public partial class TitledAuthor : ContentView
{

    public static readonly BindableProperty NameTextProperty =
    BindableProperty.Create(nameof(NameText), typeof(string), typeof(Filtrage), string.Empty);

    public string NameText
    {
        get { return (string)GetValue(NameTextProperty); }
        set { SetValue(NameTextProperty, value); }
    }

    public TitledAuthor()
	{
		InitializeComponent();
        nameText.SetBinding(Label.TextProperty, new Binding(nameof(NameText), source: this));
    }
}