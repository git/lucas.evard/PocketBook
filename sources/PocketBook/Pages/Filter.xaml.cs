using PocketBook.Applicative_VM;
using ViewModel;

namespace PocketBook;

public partial class Filter : ContentPage
{
	public NavigatorAndFilterVM NavigatorAndFilterVM { get; private set; }
    public Filter(NavigatorAndFilterVM navigatorAndFilterVM)
	{
        NavigatorAndFilterVM = navigatorAndFilterVM;
		InitializeComponent();
		BindingContext = NavigatorAndFilterVM;
	}

}