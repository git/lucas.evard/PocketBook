using CommunityToolkit.Maui.Views;
using PocketBook.Applicative_VM;

namespace PocketBook;

public partial class LaterPage : ContentPage
{
    public LaterPageVM LaterPageVM { get; private set; }

    public LaterPage(LaterPageVM laterPageVM)
	{
        LaterPageVM = laterPageVM;
		InitializeComponent();
        BindingContext = this;
	}
}