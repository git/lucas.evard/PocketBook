﻿using System.Windows.Input;
using PocketBook.Applicative_VM;

namespace PocketBook;

public partial class MainPage : ContentPage
{
	public NavigatorVM NavigateCommandBooks { get; private set; }
	public NavigatorAndFilterVM NavigatorAndFilterVM { get; private set; }
    public TousPageVM TousPageVM { get; private set; }
    public LikeVM LikeVM { get; private set; }
    public LaterPageVM LaterPageVM { get; private set; }

    public MainPage(NavigatorVM navigation, NavigatorAndFilterVM navigatorAndFilterVM, TousPageVM tousPageVM,LikeVM likeVM,LaterPageVM laterPageVM)
	{
		LikeVM = likeVM;
		LaterPageVM = laterPageVM;
		TousPageVM = tousPageVM;
		NavigatorAndFilterVM = navigatorAndFilterVM;
		NavigateCommandBooks = navigation;
        InitializeComponent();
		BindingContext = this;
	}
}

