using CommunityToolkit.Maui.Views;
using PocketBook.Applicative_VM;

namespace PocketBook;

public partial class SharePage : ContentPage
{
    public NavigatorVM NavigateCommandBooks { get; private set; }

    public SharePage(NavigatorVM navigator)
	{
		NavigateCommandBooks = navigator;
		InitializeComponent();
	}
}