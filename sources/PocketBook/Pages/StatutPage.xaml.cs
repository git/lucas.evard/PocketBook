using CommunityToolkit.Maui.Views;
using PocketBook.Applicative_VM;

namespace PocketBook;

public partial class StatutPage : ContentPage
{
    public NavigatorVM NavigateCommandBooks { get; private set; }

    public StatutPage(NavigatorVM navigator)
	{
		NavigateCommandBooks = navigator;
		InitializeComponent();
	}
}