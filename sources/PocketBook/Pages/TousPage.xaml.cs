using System.Diagnostics;
using CommunityToolkit.Maui.Core;
using CommunityToolkit.Maui.Views;
using PocketBook.Applicative_VM;
using ViewModel;

namespace PocketBook;

public partial class TousPage : ContentPage
{
    public TousPageVM TousPageVM { get; private set; }

    public TousPage(TousPageVM tousPageVM)
    {
        TousPageVM = tousPageVM;
        InitializeComponent();
        BindingContext = this;
    }
}