﻿using System;
using System.Diagnostics;
using Model;
using MyToolkitMVVM;

namespace ViewModel
{
	public class BookVM : BaseViewModel<Book>
    { 
        public string Title
		{
			get =>Model.Title;
			set =>SetProperty(Model.Title,value,v=>Model.Title=v);
		}

        public int Nbpages
        {
            get => Model.NbPages;
            set => SetProperty(Model.NbPages, value, v => Model.NbPages = v);
        }

        public float? UserRating
        {
            get => Model.UserRating;
            set => SetProperty(Model.UserRating, value,v=> Model.UserRating=v);
        }

        public string UserRatingString
        {
            get => Model.UserRating.HasValue ? Model.UserRating.ToString() : "0";
            set { }
        }

        public DateTime PublishDate
        {
            get => Model.PublishDate;
            set => SetProperty(Model.PublishDate, value, v => Model.PublishDate = v);
        }

        public string Annee
        {
            get => Model.PublishDate.ToString("yyyy");
            set { }
        }

        public Languages Language
        {
            get => Model.Language;
            set => SetProperty(Model.Language, value, v => Model.Language = v);
        }

        public string Id
        {
            get => Model.Id;
            set => SetProperty(Model.Id, value, v => Model.Id = v);
        }

        public string Image
        {
            get => Model.ImageLarge;
            set { }
        }

        public string ISBN
        {
            get => Model.ISBN13;
            set => SetProperty(Model.ISBN13, value, v => Model.ISBN13 = v);
        }

        public Status BookStatus
        {
            get => Model.Status;
            set
            {
                SetProperty(Model.Status, value, v => Model.Status = v);
            }
        }

        public List<string> BookPublishers
        {
            get => Model.Publishers;
            set
            {
                SetProperty(Model.Publishers, value, v => Model.Publishers = v);
            }
        }

        public List<Author> BookAuthors
        {
            get => Model.Authors;
            set
            {
                SetProperty(Model.Authors, value, v => Model.Authors = v);
            }
        }

        public string BookFirstAuthor { get; private set; }

        public string BookFirstPublisher { get; private set; }

        public int StarsNumber{ get; private set; }

        public BookVM(Book book): base(book)
        {
            if (BookAuthors.Count() > 0)
            {
                BookFirstAuthor = BookAuthors.First().Name;
            }
            else {
                BookFirstAuthor = "Unknown";
            }

            if (BookPublishers.Count() > 0)
            {
                BookFirstPublisher = BookPublishers.First();
            }
            else BookFirstPublisher = "Unknown";

            StarsNumber = (int)Math.Round(UserRating ?? 0);
        }
    }
}

