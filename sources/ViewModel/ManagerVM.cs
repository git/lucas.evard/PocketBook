﻿namespace ViewModel;

using System.Collections.ObjectModel;
using Model;
using MyToolkitMVVM;

public class ManagerVM : BaseViewModel<Manager>
{

    public int NBLIVREPARPAGE = 6; 

    public ReadOnlyObservableCollection<BookVM> Books { get; set; }
    private readonly ObservableCollection<BookVM> books = new ObservableCollection<BookVM>();
    public RelayCommand<string> LoadBooks { get; private set; }
    public IEnumerable<IGrouping<string, BookVM>> GroupedBooks => books.GroupBy(b => b.BookFirstAuthor).OrderBy(group => group.Key);
    public IEnumerable<IGrouping<string, BookVM>> GroupedLaterBooks => books.Where(book => book.BookStatus == Status.ToBeRead).GroupBy(book => book.BookFirstAuthor).OrderBy(group => group.Key);
    public ReadOnlyObservableCollection<Tuple<string, int>> Filters { get; private set; }
    private readonly ObservableCollection<Tuple<string, int>> filters = new ObservableCollection<Tuple<string, int>>();

    public BookVM SelectedBook
    {
        get => selectedBook;
        set => SetProperty(ref selectedBook, value);
    }

    private BookVM selectedBook;

    public ManagerVM(ILibraryManager libraryManager,IUserLibraryManager userLibraryManager)
    :this(new Manager(libraryManager,userLibraryManager))
    {}

    public async Task LoadBooksFromManager()
    {
        var result = await Model.GetBooksFromCollection(index, NBLIVREPARPAGE, "");
        NbBook = (int)result.count;
        books.Clear();

        foreach (Book book in result.books)
        {
            books.Add(new BookVM(book));
        }
        Refresh();
    }

    public async Task LoadBooksReadLaterFromManager()
    {
        var result = await Model.GetBooksFromCollection(index, NBLIVREPARPAGE, "");
        books.Clear();
        nbBook = 0;
        foreach (Book book in result.books)
        {
            if (book.Status == Status.ToBeRead)
            {
                books.Add(new BookVM(book));
                nbBook++;
            }
        }
        Refresh();
    }

    public async Task GetDatesFromCollection()
    {
        var result = await Model.GetBooksFromCollection(0, 1000);
        IEnumerable<BookVM> resDate = result.books.Select(b => new BookVM(b));

        var groupedBooksByDate = resDate.GroupBy(book => book.Annee).OrderByDescending(group => group.Key);

        var dates = groupedBooksByDate.Select(group =>
            new Tuple<string, int>(group.Key, group.Count())
        );

        filters.Clear();

        foreach (var date in dates)
        {
            filters.Add(date);
        }
        Refresh();
    }

    public async Task GetAuthorFromCollection()
    {
        var result = await Model.GetBooksFromCollection(0, 1000);
        IEnumerable<BookVM> resAuthors = result.books.Select(b => new BookVM(b));

        var groupedBooksByAuthor = resAuthors.GroupBy(book => book.BookFirstAuthor).OrderBy(group => group.Key);

        var authors = groupedBooksByAuthor.Select(group =>
            new Tuple<string, int>(group.Key, group.Count())
        );

        filters.Clear();

        foreach (var author in authors)
        {
            filters.Add(author);
        }
        Refresh();
    }

    public async Task GetMarkFromCollection()
    {
        var result = await Model.GetBooksFromCollection(0, 1000);
        IEnumerable<BookVM> resMark = result.books.Select(b => new BookVM(b));

        var groupedBooksByMark = resMark.GroupBy(book => book.UserRatingString).OrderByDescending(group => group.Key);

        var marks = groupedBooksByMark.Select(group =>
            new Tuple<string, int>(group.Key, group.Count())
        );
        filters.Clear();

        foreach (var mark in marks)
        {
            filters.Add(mark);
        }
        Refresh();
    }

    public async Task GetBooksFromAuthor(string author)
    {
        var result=await Model.GetBooksByAuthor(author,index,5);
        NbBook = (int)result.count;
        books.Clear();

        foreach (Book book in result.books)
        {
            books.Add(new BookVM(book));
        }
        Refresh();
    }

    public async Task GetBooksFromYears(string year)
    {
        var result = await Model.GetBooksFromCollection(0, 1000);
        NbBook = 0;
        books.Clear();

        foreach (Book book in result.books)
        {
            var bookVM = new BookVM(book);
            if (bookVM.Annee == year) { books.Add(bookVM); NbBook++; }
        }
        Refresh();
    }

    public async Task GetBooksFromMark(string mark)
    {
        var result = await Model.GetBooksFromCollection(0, 1000);
        NbBook = 0;
        books.Clear();

        foreach (Book book in result.books)
        {
            var bookVM = new BookVM(book);
            if (bookVM.UserRatingString == mark) { books.Add(bookVM); NbBook++; }
        }
        Refresh();
    }


    public void ChangeStatut(string statut)
    {
        switch (statut)
        {
            case "Terminé":
                SelectedBook.BookStatus = Status.Finished;
                break;
            case "Non lu":
                SelectedBook.BookStatus = Status.NotRead;
                break;
            case "À lire":
                SelectedBook.BookStatus = Status.ToBeRead;
                break;
            case "En lecture":
                SelectedBook.BookStatus = Status.Reading;
                break;
            case "Unknown":
                SelectedBook.BookStatus = Status.NotRead;
                break;
        }
        Refresh();
    }

    public void Refresh()
    {
        OnPropertyChanged(nameof(GroupedBooks));
        OnPropertyChanged(nameof(GroupedLaterBooks));
        OnPropertyChanged(nameof(filters));
        OnPropertyChanged(nameof(books));
    }

    public ManagerVM(Manager model) : base(model)
    {
        Books = new ReadOnlyObservableCollection<BookVM>(books);
        Filters= new ReadOnlyObservableCollection<Tuple<string, int>>(filters);
    }

    public async void AddBookByISBN(string isbn)
    {
        var result= Model.GetBookByISBN(isbn).Result;
        if (result != null) {
            await Model.AddBookToCollection(result.Id);
            await LoadBooksFromManager();
            Refresh();
        }
    }

    public async void RemoveSelectedBookToCollection()
    {
        var result= await Model.GetBookById(SelectedBook.Id);
        NbBook--;
        await Model.RemoveBookToCollection(result);
        books.Remove(SelectedBook);
        Refresh();
    }

    public async void AddFavoriteBook()
    {
        var result = await Model.GetBookById(SelectedBook.Id);
        await Model.AddBookInFavoriteList(result);
    }

    public async void RemoveFavoriteBook()
    {
        var result = await Model.GetBookById(SelectedBook.Id);
        NbBook--;
        await Model.DeleteBookInFavoriteList(result);
        books.Remove(SelectedBook);
        Refresh();
    }

    public async Task GetFavoriteBooks()
    {
        var result = await Model.GetAllBooksFromFavorite(index, NBLIVREPARPAGE);
        NbBook = (int)result.count;
        books.Clear();

        foreach (Book book in result.books)
        {
            books.Add(new BookVM(book));
        }
        Refresh();
    }

    public void MoveBook(string action)
    {
        switch (action)
        {
            case "Mettre à la poubelle":
                RemoveSelectedBookToCollection();
                break;
            case "Déplacer en favoris":
                AddFavoriteBook();
                break;
            case "Supprimer des favoris":
                RemoveFavoriteBook();
                break;
        }
    }

    public int Index
    {
        get => index;
        set
        {
            SetProperty(ref index, value);
        }
    }

    private int index;

    public int Count
    {
        get => count;
        set => SetProperty(ref count, value);

    }
    private int count;


    public int NbBook
    {
        get => nbBook;
        set => SetProperty(ref nbBook, value);
    }

    private int nbBook;

}

